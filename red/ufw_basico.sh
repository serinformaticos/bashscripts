#!/bin/bash

# comandos básicos para configuración básica del firewal UFW

# apt-get install ufw

# habilitar IPV6
# nano /etc/default/ufw
# IPV6=yes

# activa ufw
ufw enable

# deniega todo tráfico entrante
ufw default deny incoming

# permite TODO tráfico de salida
ufw default allow outgoing

# habilita entrada puerto 22 po rTCP
ufw allow 22/tcp

# habilita entrada puerto 443 po rTCP
ufw allow 443/tcp

# recarga nuevas reglas
ufw reload

# permite acceso a la red
# ufw allow from 192.168.255.255

# permite acceso a una sola ip
# ufw allow from 192.168.1.2/32

# permite rango de puertos
# ufw allow 1000:2000/udp

# Listado de reglas numeradas
# ufw status numbered

# Eliminar regla numerada
# ufw delete XX

# elimina todo
# ufw reset

# deshabilitar
# ufw disable

# muestra reglas
ufw status status
ufw status verbose


