#!/bin/bash
. /root/scripts/./funciones.lib

# escanea red, guarda direccion MAC, y cuando la hora es la 23.00, envia el log como adjunto al email

interface="bond0"
red="172.26.2."
log="/root/logs/Red-$fecha.log"
asunto="$hostname - Red3 Bloquear"

#Hora a la que envía email
horaEnvio="23.00"

echo "$hora - $horaEnvio"

touch $log

arp-scan --interface $interface -l | \
        grep $red'.*' | \
        awk '{print $1,"\t",$2,"\t",$3}' >> $log

sort -u $log > $log.tmp
mv $log.tmp $log


if [[ $hora == $horaEnvio ]];
then
        tar cvzf $log.tgz $log
        echo $asunto | mutt -s "$asunto $fecha" -a "$log.tgz" -- $email1
else
        echo "no es la hora de envio"
fi
