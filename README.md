Listado de Bash Scripts creadas y recolectadas para gestión, administarción y copias de seguridad para Servidores y Ordenadores Linux a lo largo de los últimos 15 años.

Muchos de estos scripts están orientados a Debian, distribución que utilizamos para los servidores.

Generalmente son scripts agnósticas a la distribución donde se ejecuten, pero puede encontrarse scripts escritas para funcionar especificamente para Debian o derivados.

La ubicación predeterminada de nuestros scripts es "/root/scripts/"

El Equipo de SerInformaticos
