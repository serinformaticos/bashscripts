#!/bin/bash
. /root/scripts/./funciones.lib

#Constantes
destino="/tmp"
msn="Clam-Av Actualizado"

echo "Actualizando archivos desde Internet"
mkdir $destino

wget http://db.local.clamav.net/daily.cvd -P$destino/
wget http://db.local.clamav.net/main.cvd -P$destino/

echo "actualizando bases"
clamscan -d $destino

twitter

rm $destino/daily.cvd
rm $destino/main.cvd
